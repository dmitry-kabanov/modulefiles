require("sandbox")
local unpack = unpack or table.unpack
SiteRootDir = "/opt/apps"
DefaultsDir = os.getenv("HOME") .. "/dev/modulefiles"

function loadPkgDefaults(levels)
    local pkg = {}
    local status
    local msg
    local whole

    ------------------------------------------------------------
    -- Fill default values
    pkg.name         = myModuleName()
    pkg.version      = myModuleVersion()
    pkg.id           = myModuleFullName()
    pkg.displayName = pkg.name
    pkg.url          = ""
    pkg.license      = ""
    pkg.category     = ""
    pkg.keywords     = ""
    pkg.description  = ""
    pkg.help         = ""

    ------------------------------------------------------------
    -- build package prefix from modulefile location
    local hierA      = hierarchyA(pkg.id, levels)
    local a          = {}
    a[#a+1]          = SiteRootDir
    for i = levels,1,-1 do
       a[#a+1] = hierA[i]:gsub("/","-"):gsub("%.","_")
    end
    a[#a+1]          = pkg.id
    pkg.prefix       = pathJoin(unpack(a))


    ------------------------------------------------------------
    -- Read default package description file

    local folder = false
    local filename = "-description.lua"
    if (levels == 1) then
        folder = "Compiler/"
    elseif (levels == 2) then
        folder = "MPI/"
    end
    local fn         = pathJoin(DefaultsDir, folder .. pkg.name .. filename)
    local f          = io.open(fn)
    local whole      = false
    local status     = false
    local msg        = "Empty file"
    if (f) then
        whole = f:read("*all")
        f:close()
    end

    ------------------------------------------------------------
    -- Evaluate string from package description file through
    -- sandbox_run for safety checks.
    if (whole) then
        status, msg = sandbox_run(whole)
    end

    if (not status) then
        LmodError("Unable to load file: ", fn, ": ", msg, "\n")
    end

    for k,v in pairs(msg) do
       pkg[k] = v
    end
    return pkg
end

function setPkgInfo(pkg)
    help(pkg.help)
    whatis("Name: "        .. pkg.displayName)
    whatis("Version: "     .. pkg.version)
    whatis("Module: "      .. pkg.id)
    whatis("Category: "    .. pkg.category)
    whatis("Keyword: "     .. pkg.keywords)
    whatis("URL: "         .. pkg.url)
    whatis("License: "     .. pkg.license)
    whatis("Description: " .. pkg.description)
end

function prependModulePath(subdir)
   local mroot = os.getenv("MODULEPATH_ROOT")
   local mdir  = pathJoin(mroot, subdir)
   prepend_path("MODULEPATH", mdir)
end

function appendModulePath(subdir)
   local mroot = os.getenv("MODULEPATH_ROOT")
   local mdir  = pathJoin(mroot, subdir)
   append_path("MODULEPATH", mdir)
end

sandbox_registration{ loadPkgDefaults = loadPkgDefaults, 
                      setPkgInfo = setPkgInfo,
                      prependModulePath    = prependModulePath,
                      appendModulePath     = appendModulePath }
