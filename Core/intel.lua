help(
[[
Intel® Parallel Studio XE 2013 update 4.
What’s Included:
Intel® C,C++ and Fortran Compilers
Intel® Threading Building Blocks 4.1
Intel® Math Kernel Library 11.0
Intel® Integrated Performance Primitives 7.1
Intel® Advisor XE 2013
Intel® VTune™ Amplifier XE 2013
Intel® Inspector XE 2013
Static Analysis
]])

whatis("Description: Intel® Parallel Studio XE 2013 update 4")
whatis("URL:         http://software.intel.com/en-us/intel-parallel-studio-xe/")

local version = "2013u4"
local base = pathJoin("/opt/apps/intel", version)
local installDir=pathJoin(base, "composer_xe_2013.5.192")
local mklRoot = pathJoin(installDir, "mkl")
local tbbRoot = pathJoin(installDir, "tbb")
local ippRoot = pathJoin(installDir, "ipp")

-- IDB settings
prepend_path("NLSPATH", pathJoin(installDir, "debugger/intel64/locale/%l_%t/%N"))
prepend_path("PATH", pathJoin(installDir, "debugger/gui/intel64"))
prepend_path("PATH", pathJoin(installDir, "bin/intel64_mic"))
prepend_path("PATH", pathJoin(installDir, "bin/intel64"))
pushenv("IDB_HOME", pathJoin(installDir, "bin/intel64"))

-- TBB settings
prepend_path("LD_LIBRARY_PATH", pathJoin(tbbRoot, "lib/intel64/gcc4.4"))
prepend_path("LIBRARY_PATH", pathJoin(tbbRoot, "lib/intel64/gcc4.4"))
prepend_path("CPATH", pathJoin(tbbRoot, "include"))

-- MKL settings
prepend_path("LD_LIBRARY_PATH", pathJoin(mklRoot, "lib/intel64"))
prepend_path("LD_LIBRARY_PATH", pathJoin(installDir, "compiler/lib/intel64"))
prepend_path("LIBRARY_PATH", pathJoin(mklRoot, "lib/intel64"))
prepend_path("LIBRARY_PATH", pathJoin(installDir, "compiler/lib/intel64"))
prepend_path("NLSPATH", pathJoin(mklRoot, "lib/intel64/locale/%l_%t/%N"))
prepend_path("MANPATH", pathJoin(installDir, "man/en_US"))
prepend_path("INCLUDE", pathJoin(mklRoot, "include"))
prepend_path("CPATH", pathJoin(mklRoot, "include"))
pushenv("MKLROOT", mklRoot)

-- IPP settings
prepend_path("LD_LIBRARY_PATH", pathJoin(ippRoot, "lib/intel64"))
prepend_path("LD_LIBRARY_PATH", pathJoin(ippRoot, "../compiler/lib/intel64"))
prepend_path("LIBRARY_PATH", pathJoin(ippRoot, "lib/intel64"))
prepend_path("LIBRARY_PATH", pathJoin(ippRoot, "../compiler/lib/intel64"))
prepend_path("NLSPATH", pathJoin(ippRoot, "lib/intel64/locale/%l_%t/%N"))

-- Compilers settings
prepend_path("PATH", pathJoin(installDir, "mpirt/bin/intel64"))
prepend_path("PATH", pathJoin(installDir, "bin/intel64"))
prepend_path("LD_LIBRARY_PATH", pathJoin(installDir, "mpirt/lib/intel64"))
prepend_path("LD_LIBRARY_PATH", pathJoin(installDir, "compiler/lib/intel64"))
prepend_path("LIBRARY_PATH", pathJoin(installDir, "compiler/lib/intel64"))
prepend_path("NLSPATH", pathJoin(installDir, "compiler/lib/intel64/locale/%l_%t/%N"))
pushenv("CC", "icc")
pushenv("CXX", "icpc")
pushenv("F77", "ifort")
pushenv("FC", "ifort")
pushenv("F90", "ifort")

pushenv("INTEL_LICENSE_FILE", "/opt/intel/licenses")

-- Setup Modulepath for packages built by this compiler
local mroot = os.getenv("MODULEPATH_ROOT")
local mdir  = pathJoin(mroot,"Compiler/intel", version)
prepend_path("MODULEPATH", mdir)
family("compiler")
