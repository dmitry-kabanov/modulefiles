help(
[[
This module loads the GNU Compiler Collection suite, version 4.6.4.
This provides a Fortran, C, and C++ compilers.
]])

local version = "4.6.4"
local base = pathJoin("/opt/apps/gcc", version)

prepend_path("PATH", pathJoin(base, "bin"))
prepend_path("LIBRARY_PATH", pathJoin(base, "lib64"))
prepend_path("LD_LIBRARY_PATH", pathJoin(base, "lib64"))
prepend_path("MANPATH", pathJoin(base, "share"))

pushenv("CC", "gcc")
pushenv("CXX", "g++")
pushenv("F77", "gfortran")
pushenv("FC", "gfortran")
pushenv("F90", "gfortran")

-- Setup Modulepath for packages built by this compiler
local mroot = os.getenv("MODULEPATH_ROOT")
local mdir  = pathJoin(mroot,"Compiler/gcc", version)
prepend_path("MODULEPATH", mdir)
family("compiler")
