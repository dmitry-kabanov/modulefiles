local pkg = {}

pkg.displayName = "Zlib Library"
pkg.category = "development"
pkg.keywords = "library"
pkg.url = "zlib.net"
pkg.license = "GPL"
pkg.description = "zlib compression library"

pkg.help = [[
Zlib is general purpose compression library.
To use Zlib library in your code, compile your source code with 
the following option:
    -I${APPS_ZLIB_CPPFLAGS}

Add the following options to the link step for C codes:
    -L${APPS_ZLIB_LDFLAGS} -l${APPS_ZLIB_LIBS}
]]

return pkg
