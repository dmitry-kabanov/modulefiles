help([[
LAPACK library, version 3.4.2
This LAPACK implementation is built against OpenBLAS 0.2.6.
See http://www.netlib.org/lapack/
]])

whatis("Description: LAPACK library, version 3.4.2")
whatis("This LAPACK implementation is built against OpenBLAS 0.2.6")
whatis("URL: http://www.netlib.org/lapack/")

prereq("openblas/0.2.6")

local version = "3.4.2"
local compilerName = "gcc"
local compilerVersion = "4.6.4"
local category = compilerName .. "-" .. compilerVersion
local base = pathJoin("/opt/apps", category, "netlib-lapack", version)

prepend_path("LD_LIBRARY_PATH", pathJoin(base, "lib"))
