local pkg = loadPkgDefaults(1)
setPkgInfo(pkg)

prepend_path("LD_LIBRARY_PATH", pathJoin(pkg.prefix, "lib"))
prepend_path("MANPATH",         pathJoin(pkg.prefix, "share/man"))

pushenv("APPS_ZLIB_CPPFLAGS", pathJoin(pkg.prefix, "include"))
pushenv("APPS_ZLIB_LDFLAGS", pathJoin(pkg.prefix, "lib"))
pushenv("APPS_ZLIB_LIBS", "z")
