local pkg = {}

pkg.displayName = "OpenMPI Library"
pkg.category = "development"
pkg.keywords = "library"
pkg.url = "open-mpi.org"
pkg.license = "New BSD"
pkg.description = "MPI library"

pkg.help = [[
MPI stands for the Message Passing Interface. Written by the MPI Forum (a large 
committee comprising of a cross-section between industry and research 
representatives), MPI is a standardized API typically used for parallel and/or 
distributed computing. The MPI standard is comprised of 2 documents: MPI-1 
(published in 1994) and MPI-2 (published in 1996). MPI-2 is, for the most part, 
additions and extensions to the original MPI-1 specification.

The MPI-1 and MPI-2 documents can be downloaded from the official MPI Forum 
web site: http://www.mpi-forum.org/.

Open MPI is an open source, freely available implementation of both the MPI-1 
and MPI-2 documents. The Open MPI software achieves high performance; the 
Open MPI project is quite receptive to community input.

To compile your MPI application, use wrapper scripts, for example:
    $ mpicc my_mpi_application.c -o my_mpi_application

Wrapper scripts are:
mpicc - wrapper script for C compiler
mpicxx - wrapper script for C++ compiler
mpif77 - wrapper script for Fortran 77 compiler
mpif90 - wrapper script for Fortran 90 compiler
]]

return pkg
