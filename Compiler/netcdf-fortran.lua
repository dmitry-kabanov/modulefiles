local version = "4.2"
local compilerName = "gcc"
local compilerVersion = "4.6.4"
local category = compilerName .. "-" .. compilerVersion
local base = pathJoin("/opt/apps", category, "netcdf-fortran", version)

prepend_path("PATH", pathJoin(base, "bin"))
prepend_path("LD_LIBRARY_PATH", pathJoin(base, "lib"))
prepend_path("MANPATH", pathJoin(base, "share"))

prereq("netcdf/4.3.0")

help([[
NetCDF Fortran library, version ]] .. version .. [[
This library allow to work with NetCDF files in Fortran programs.
See http://www.unidata.ucar.edu/software/netcdf/
]])

whatis("Description: NetCDF Fortran library, version " .. version)
whatis("URL: http://www.unidata.ucar.edu/software/netcdf/")
