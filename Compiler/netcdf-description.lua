local pkg = {}

pkg.displayName = "NetCDF Library"
pkg.category = "development"
pkg.keywords = "library"
pkg.url = "http://www.unidata.ucar.edu/software/netcdf/"
pkg.license = "GPL"
pkg.description = "Network Common Data Form (NetCDF) library"

pkg.help = [[
NetCDF is a set of software libraries and self-describing, machine-independent 
data formats that support the creation, access, and sharing of array-oriented 
scientific data.
The installed version of NetCDF provides only netCDF classic and 64-bit offset 
formats!

To use NetCDF library in your code, compile your source code with 
the following option:
    -I${APPS_NETCDF_CPPFLAGS}

Add the following options to the link step for C codes:
    -L${APPS_NETCDF_LDFLAGS} -l${APPS_NETCDF_LIBS}
]]

return pkg
