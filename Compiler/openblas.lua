help([[
OpenBLAS library, version 0.2.6
An optimized, open-source library implementing the BLAS subrountines.
See http://xianyi.github.io/OpenBLAS/
]])

whatis("Description: OpenBLAS library, version 0.2.6")
whatis("An optimized, open-source library implementing the BLAS subrountines.")
whatis("URL: http://xianyi.github.io/OpenBLAS/")

local version = "0.2.6"
local compilerName = "gcc"
local compilerVersion = "4.6.4"
local category = compilerName .. "-" .. compilerVersion
local base = pathJoin("/opt/apps", category, "openblas", version)

prepend_path("LD_LIBRARY_PATH", pathJoin(base, "lib"))
