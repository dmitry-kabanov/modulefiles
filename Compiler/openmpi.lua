local pkg = loadPkgDefaults(1)
setPkgInfo(pkg)

prepend_path("PATH", pathJoin(pkg.prefix, "bin"))
prepend_path("LD_LIBRARY_PATH", pathJoin(base, "lib"))
prepend_path("MANPATH", pathJoin(pkg.prefix, "share/man"))

-- Setup Modulepath for packages built with this MPI library.
local hierA     = hierarchyA(myModuleFullName(), 1)
local compilerName = hierA[1]:gsub("-", "/")
prependModulePath(pathJoin("MPI", compilerName, pkg.id))
family("mpi")
