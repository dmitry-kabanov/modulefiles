help([[
Sundials library, version 2.5.0
SUite of Nonlinear and DIfferential/ALgebraic equation Solvers
See http://computation.llnl.gov/casc/sundials/main.html
]])

whatis("Description: Sundials library, version 2.5.0")
whatis("SUite of Nonlinear and DIfferential/ALgebraic equation Solvers.")
whatis("URL: http://computation.llnl.gov/casc/sundials/main.html")

local version = "2.5.0"
local compilerName = "gcc"
local compilerVersion = "4.6.4"
local mpiName = "openmpi"
local mpiVersion = "1.6.5"
local category = compilerName .. "-" .. compilerVersion
local subcategory = mpiName .. "-" .. mpiVersion
local base = pathJoin("/opt/apps", category, subcategory, "sundials", version)

prepend_path("PATH", pathJoin(base, "bin"))
prepend_path("LD_LIBRARY_PATH", pathJoin(base, "lib"))
