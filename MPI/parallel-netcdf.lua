local version = "1.3.1"
local compilerName = "gcc"
local compilerVersion = "4.6.4"
local mpiName = "openmpi"
local mpiVersion = "1.6.5"
local category = compilerName .. "-" .. compilerVersion
local subcategory = mpiName .. "-" .. mpiVersion
local base = pathJoin("/opt/apps", category, subcategory, "parallel-netcdf", version)

prepend_path("PATH", pathJoin(base, "bin"))
prepend_path("CPATH", pathJoin(base, "include"))
prepend_path("LIBRARY_PATH", pathJoin(base, "lib"))
prepend_path("LD_LIBRARY_PATH", pathJoin(base, "lib"))
prepend_path("MANPATH", pathJoin(base, "man"))

help([[
Parallel netCDF (PnetCDF) library from Argonne National Laboratory, version ]]
.. version .. [[ 
Parallel netCDF is a library providing high-performance I/O while 
still maintaining file-format compatibility with Unidata's NetCDF.
See https://trac.mcs.anl.gov/projects/parallel-netcdf
]])

whatis("Description: Parallel netCDF (PnetCDF) library, version " .. version)
whatis("URL: https://trac.mcs.anl.gov/projects/parallel-netcdf")

